package jp.co.uniquevision.imageeditor.editor;

import android.content.Context;
import android.os.Bundle;
import com.google.common.eventbus.EventBus;

import java.text.DecimalFormat;

import jp.co.uniquevision.imageeditor.editor.TextDisposition;

/**
 * Created by tajima-junpei on 13/12/10.
 * 現在のエディタの設定を表すクラス
 * SharedPreferencesからのエディタ関連の設定の読み取りと
 * 書き出しも担う
 */
public class CurrentSetting {

  // TODO onSaveInstanceState対応

  public class SettingChangeEvent {
    public static final String ARG_KEY_FONT_SIZE = "fontSize";
    public static final String ARG_KEY_FONT_COLOR = "fontColor";
    public static final String ARG_KEY_TEXT_DISPOSITION = "textDisposition";

    private Bundle setting;

    public SettingChangeEvent(int fontSize, int fontColor, int textDisposition) {
      setting = new Bundle();
      setting.putInt(ARG_KEY_FONT_SIZE, fontSize);
      setting.putInt(ARG_KEY_FONT_COLOR, fontColor);
      setting.putInt(ARG_KEY_TEXT_DISPOSITION, textDisposition);
    }

    public Bundle getSetting() {
      return setting;
    }
  }

  public static final int DEFAULT_FONT_SIZE = 10;

  // 現在のフォントサイズ
  private int fontSize = DEFAULT_FONT_SIZE;

  // 現在のフォントカラー(android.graphics.Colorの定数値(黒か白))
  private int fontColor = 0;

  // 現在の配置(text_dispotisionsリソースの添字)
  private int textDispotision = TextDispositionManager.TOP_LEFT;

  private Context context; // inject対象
  private EventBus eventBus; // inject対象

  public CurrentSetting(Context context, EventBus eventBus) {
    this.context = context;
    this.eventBus = eventBus;

    if (isSavedToSharedPreferences()) {
      restoreFromSharedPreferences();
    }
  }

  private void restoreFromSharedPreferences() {
    // TODO implement
  }

  private boolean isSavedToSharedPreferences() {
    // TODO implement
    return false;
  }

  public void saveToSharedPreferences() {
    // TODO implement
  }

  public int getFontSize() {
    return fontSize;
  }

  public void setFontSize(int fontSize) {
    this.fontSize = fontSize;
    postEvent();
  }

  public int getFontColor() {
    return fontColor;
  }

  public void setFontColor(int fontColor) {
    this.fontColor = fontColor;
    postEvent();
  }

  public int getTextDispotision() {
    return textDispotision;
  }

  public void setTextDispotision(int textDispotision) {
    this.textDispotision = textDispotision;
    postEvent();
  }

  private void postEvent() {
    eventBus.post(new SettingChangeEvent(fontSize, fontColor, textDispotision));
  }

}
