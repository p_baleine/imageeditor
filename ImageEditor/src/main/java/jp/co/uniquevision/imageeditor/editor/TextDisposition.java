package jp.co.uniquevision.imageeditor.editor;

import android.os.Parcel;
import android.os.Parcelable;

/**
* Created by tajima-junpei on 13/12/11.
*/
public class TextDisposition implements Parcelable {
  public int x;
  public int y;

  public TextDisposition() {
    // gson用
  }

  public TextDisposition(Parcel parcel) {
    x = parcel.readInt();
    y = parcel.readInt();
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel parcel, int i) {
    parcel.writeInt(x);
    parcel.writeInt(y);
  }

  public static final Creator<TextDisposition> CREATOR =
      new Creator<TextDisposition>() {

        @Override
        public TextDisposition createFromParcel(Parcel parcel) {
          return new TextDisposition(parcel);
        }

        @Override
        public TextDisposition[] newArray(int i) {
          return new TextDisposition[0];
        }
      };
}
