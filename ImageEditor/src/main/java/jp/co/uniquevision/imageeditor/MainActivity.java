package jp.co.uniquevision.imageeditor;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import jp.co.uniquevision.imageeditor.editor.CurrentSetting;
import jp.co.uniquevision.imageeditor.editor.TextDisposition;
import jp.co.uniquevision.imageeditor.editor.TextDispositionManager;

public class MainActivity extends Activity {

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    if (savedInstanceState == null) {
      getFragmentManager().beginTransaction()
          .add(R.id.container, new PlaceholderFragment())
          .commit();
    }
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.main, menu);
    return true;
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();
    if (id == R.id.action_settings) {
      return true;
    }
    return super.onOptionsItemSelected(item);
  }

  /**
   * A placeholder fragment containing a simple view.
   */
  public static class PlaceholderFragment extends Fragment implements TextWatcher {

    private static final int REQUEST_CODE = 1;

    @InjectView(R.id.font_size) EditText fontSizeEditText;
    @InjectView(R.id.image) ImageView imageView;
    private Bitmap bitmap;
    private Bitmap copy;
    private CurrentSetting setting;
    private EventBus eventBus;
    private TextDispositionManager textDispositionManager;

    public PlaceholderFragment() {
      eventBus = new EventBus();
      eventBus.register(this);
      setting = new CurrentSetting(getActivity(), eventBus);
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
      View rootView = inflater.inflate(R.layout.fragment_main, container, false);
      ButterKnife.inject(this, rootView);

      fontSizeEditText.addTextChangedListener(this);
      try {
        textDispositionManager = new TextDispositionManager(getActivity());
      } catch (UnsupportedEncodingException e) {
        e.printStackTrace();
      }

      return rootView;
    }

    @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
      if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
        try {
          InputStream stream = getActivity().getContentResolver().openInputStream(data.getData());
          BitmapFactory.Options options = new BitmapFactory.Options();
          options.inPreferredConfig = Bitmap.Config.RGB_565;
          bitmap = BitmapFactory.decodeStream(stream, null, options);
          stream.close();

          rerender(
              setting.getFontSize(),
              setting.getFontColor(),
              setting.getTextDispotision()
          );

          popupBitmapMetrics();
        } catch (FileNotFoundException e) {
          e.printStackTrace();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
      super.onActivityResult(requestCode, resultCode, data);
    }

    @Subscribe public void applySettingChange(CurrentSetting.SettingChangeEvent e) {
      Bundle data = e.getSetting();
      rerender(
          data.getInt(CurrentSetting.SettingChangeEvent.ARG_KEY_FONT_SIZE),
          data.getInt(CurrentSetting.SettingChangeEvent.ARG_KEY_FONT_COLOR),
          data.getInt(CurrentSetting.SettingChangeEvent.ARG_KEY_TEXT_DISPOSITION)
      );
    }

    private void rerender(int fontSize, int fontColor, int textDispositionIdx) {
      BitmapFactory.Options options = new BitmapFactory.Options();
      options.inPreferredConfig = Bitmap.Config.RGB_565;

      // お絵かき
      copy = bitmap.copy(Bitmap.Config.RGB_565, true);
      Canvas canvas = new Canvas(copy);
//      Paint paint = new Paint();
//      paint.setColor(fontColor);
//      paint.setStrokeWidth(12);
//      paint.setTextSize(fontSize);


      TextPaint paint = new TextPaint();
      paint.setColor(fontColor);
      paint.setStrokeWidth(12);
      paint.setTextSize(fontSize);

      float x = 0;
      float y = 0;
      float margin = 10;
      switch (textDispositionIdx) {
        case 0: // 左上
          x = margin;
          y = 0 - paint.ascent() + margin;
          break;
        case 1: // 右上
          x = bitmap.getWidth() - margin;
          y = 0 - paint.ascent() + margin;
          paint.setTextAlign(Paint.Align.RIGHT);
          break;
        case 2: // 左下
          x = margin;
          y = bitmap.getHeight() - paint.descent() - margin;
          break;
        case 3: // 右下
          x = bitmap.getWidth() - margin;
          y = bitmap.getHeight() - paint.descent() - margin;
          paint.setTextAlign(Paint.Align.RIGHT);
          break;
      }

//      TextDisposition textDisposition = textDispositionManager.get(textDispositionIdx);
      canvas.drawText("piyo@pipp", x, y, paint);

      imageView.setImageBitmap(copy);
    }

    private void popupBitmapMetrics() {
      String metrics = String.format("Density: %1$d\nHeight: %2$d\nWidth: %3$d",
          bitmap.getDensity(), bitmap.getHeight(), bitmap.getWidth());
      Toast.makeText(getActivity(), metrics, Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.select) public void startAlbum() {
      Intent intent = new Intent();
      intent.setType("image/*");
      intent.setAction(Intent.ACTION_GET_CONTENT);
      intent.addCategory(Intent.CATEGORY_OPENABLE);
      startActivityForResult(intent, REQUEST_CODE);
    }

    @OnClick(R.id.top_left) void moveTextTopLeft() {
      setting.setTextDispotision(0);
    }

    @OnClick(R.id.top_right) void moveTextTopRight() {
      setting.setTextDispotision(1);
    }

    @OnClick(R.id.bottom_left) void moveTextBottomLeft() {
      setting.setTextDispotision(2);
    }

    @OnClick(R.id.bottom_right) void moveTextBottomRight() {
      setting.setTextDispotision(3);
    }

    @OnClick(R.id.to_white) void colorTextWhite() {
      setting.setFontColor(Color.WHITE);
    }

    @OnClick(R.id.to_black) void colorTextBlack() {
      setting.setFontColor(Color.BLACK);
    }

    @OnClick(R.id.save) void saveImage() {
      try {
        Uri uri = getOutputMediaFileUri();
        FileOutputStream out = new FileOutputStream(uri.getPath());
        copy.compress(Bitmap.CompressFormat.JPEG, 100, out);
        out.close();
        // ギャラリー等に通知する
        Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        intent.setData(uri);
        getActivity().sendBroadcast(intent);
      } catch (FileNotFoundException e) {
        throw new RuntimeException(e);
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    }

    private static Uri getOutputMediaFileUri() {
      return Uri.fromFile(getOutputMediaFile());
    }

    private static File getOutputMediaFile() {
      File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
          Environment.DIRECTORY_PICTURES), "CooperateWithCamera");

      // まだない場合ストレージディレクトリを作成する
      if (!mediaStorageDir.exists()) {
        if (!mediaStorageDir.mkdirs()) {
          Log.d("CooperateWithCamera", "failed to create directory");
          return null;
        }
      }

      // メディアファイル名の作成
      String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
      File mediaFile = new File(mediaStorageDir.getPath() +
          File.separator + "IMG_" + timeStamp + ".jpeg");

      return mediaFile;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    @Override
    public void afterTextChanged(Editable editable) {
      // フォントサイズが入力された時
      String size = fontSizeEditText.getText().toString();
      if (!TextUtils.isEmpty(size)) {
        setting.setFontSize(Integer.parseInt(size));
      }
    }
  }

}
