package jp.co.uniquevision.imageeditor.editor;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.List;

import jp.co.uniquevision.imageeditor.R;

/**
 * Created by tajima-junpei on 13/12/11.
 * リソースに定義してある文言の配置設定一覧を元に
 * エディタに提供する文言の配置設定の管理を行う
 */
public class TextDispositionManager {

  public static final int TOP_LEFT = 0;
  public static final int TOP_RIGHT = TOP_LEFT + 1;
  public static final int BOTTOM_LEFT = TOP_LEFT + 2;
  public static final int BOTTOM_RIGHT = TOP_LEFT + 3;

  private Context context;
  private List<TextDisposition> textDispositions;

  public TextDispositionManager(Context context) throws UnsupportedEncodingException {
    this.context = context;
    InputStream jsonStream = context.getResources().openRawResource(R.raw.image_editor_text_dispotisions);
    JsonReader reader = new JsonReader(new InputStreamReader(jsonStream, "UTF-8"));
    Type type = new TypeToken<List<TextDisposition>>(){}.getType();
    textDispositions = new Gson().fromJson(reader, type);
  }

  public TextDisposition get(int index) {
    if (index >= textDispositions.size()) {
      throw new IndexOutOfBoundsException("テキスト配置設定のリソース範囲外");
    }

    return textDispositions.get(index);
  }
}
